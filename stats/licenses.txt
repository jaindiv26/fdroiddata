GPL-3.0-only 1104
Apache-2.0 673
GPL-3.0-or-later 452
MIT 356
GPL-2.0-only 146
GPL-2.0-or-later 109
BSD-3-Clause 49
AGPL-3.0-only 33
MPL-2.0 26
AGPL-3.0-or-later 21
LGPL-3.0-only 19
WTFPL 17
Unlicense 15
BSD-2-Clause-FreeBSD 14
PublicDomain 12
ISC 8
LGPL-2.1-only 6
Artistic-2.0 4
LGPL-2.1-or-later 4
BSD-2-Clause 3
CC-BY-4.0 3
CC-BY-SA-4.0 3
EPL-1.0 3
Beerware 2
CC-BY-NC-3.0 2
CC-BY-SA-3.0 2
CC0-1.0 2
MirOS 2
X11 2
Zlib 2
0BSD 1
AML 1
EUPL-1.1 1
EUPL-1.2 1
Fair 1
LGPL-3.0-or-later 1
MIT-CMU 1
MPL-1.1 1
NCSA 1
NPOSL-3.0 1
XSkat 1
